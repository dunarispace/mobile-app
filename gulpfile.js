/**
 * Created by kbartczak on 23.03.2017.
 */

/** Include plugins **/
var gulp = require('gulp'),
    rev = require('gulp-rev-mtime'),
    sourcemaps = require('gulp-sourcemaps'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    sass = require('gulp-sass'),
    cleanCSS = require('gulp-clean-css'),
    gulpSequence = require('gulp-sequence'),
    imagemin = require('gulp-imagemin'),
    critical = require('critical');

/** Helpers */
var runTimestamp = Math.round(Date.now() / 1000);

var config = {
    templateName: 'absynt',
    url:'http://www-threesisters-kb.devphp.m4u.pl',
    paths: {
        scripts: [

            /* jquery */
            './src/js/lib/jquery.min.js',
            './src/js/lib/jquery.autocomplete.js',

            /* materialize */
            './src/js/materialize/bin/materialize.min.js',

            /* m4u components*/

            // './src/scripts/lib/mCustomScrollbar/jquery.mousewheel.js',
            // './src/scripts/lib/mCustomScrollbar/jquery.mousewheel-3.0.6.js',

            /* customes */
            './src/js/app.js',
            /*'./src/scripts/plugins/*.js',
            './src/scripts/scripts.js'*/
        ],
        styles: ['./src/sass/main.scss'],
        images: ['./src/gfx/**/*'],
        svg: ['./src/svg/**/*'],
        fonts: ['./src/fonts/**/*']
    },
    critical: [
        {"template": "home",        "url": "/"},
        {"template": "category",    "url": "/pl/odziez-damska"},
        {"template": "product",     "url": "/pl/odziez-damska/sukienka-letnia-coco-3,p-4"},
        {"template": "news-list",   "url": "/pl/artykuly/aktualnosci"},
        {"template": "news-single", "url": "/pl/artykuly/aktualnosci/test-1"}
    ],
};

/** Watch Files For Changes */
gulp.task('watch', function () {
    gulp.watch('./src/js/**/*.js', ['build-js']);
    gulp.watch('./src/sass/**/*.scss', ['build-css']);
    //gulp.watch('./dist/**/*', ['rev']);
});

/** Build our application - Development environment **/
gulp.task('dist-dev', gulpSequence(
    ['build-css', 'build-js', 'copy-svg', 'optimize-images']
));

/** Build our application - Production environment **/
gulp.task('dist-prod', gulpSequence(
    ['minify-js', 'minify-css', 'copy-svg', 'optimize-images'],
    'critical'
));

/** Copying Fonts to Dist */
gulp.task('copy-fonts', function () {
    return gulp.src(config.paths.fonts)
        .pipe(gulp.dest('www/dist/fonts'))
});

/** Compile (S)CSS file */
gulp.task('build-css', function () {
    return gulp.src(config.paths.styles)
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./www/dist/styles/'));
});

/** Minify CSS-ss */
gulp.task('minify-css', ['build-css'], function () {
    return gulp.src('./www/dist/styles/**/*.css')
        .pipe(cleanCSS())
        .pipe(gulp.dest('www/dist/styles'));
});


/** Concatenate core JS */
gulp.task('build-js', function () {
    return gulp.src(config.paths.scripts)
        .pipe(concat('main.js'))
        .pipe(gulp.dest('www/dist/js'));
});

/** Minify js-ss */
gulp.task('minify-js', ['build-js'], function () {
    return gulp.src('./www/dist/js/**/*.js')
        .pipe(uglify())
        .pipe(gulp.dest('www/dist/js'));
});

/** Copy and optimize Images to Dist **/
gulp.task('optimize-images', function () {
    gulp.src(config.paths.images)
        .pipe(imagemin())
        .pipe(gulp.dest('www/dist/gfx'))
});

/** Copying svg to Dist */
gulp.task('copy-svg', function () {
    return gulp.src(config.paths.svg)
        .pipe(gulp.dest('dist/gfx'))
});