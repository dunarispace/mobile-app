$(document).ready(function() {
    $('ul.tabs').tabs(
        {
            swipeable: true
        }
    );

/*********************** wysyłanie danych ***********************/
    /*setTimeout(function(){
        var o = document.getElementsByTagName('iframe')[0];
        o.contentWindow.postMessage('Hello world', '*');
    }, 500);*/


    /*************** resize listener **************************/
    $( window ).resize(function() {
        resizeListener()
    });

    $('.logo').click(function(){
        $(".tabs .home a").trigger("click");
        $(".swipes .preloader").show(0)
    })
    $("body")
        .on("click", ".dropdown-content li a", function() {
            $(".tabs .home a").trigger("click");
            $(".swipes .preloader").show(0)
        })
        .on("click", ".logo", function() {
            $(".tabs .home a").trigger("click");
            $(".swipes .preloader").show(0);
        })
        .on("click", ".search a, .close-search", function() {
            $(".search-float").toggleClass("show");
            $("#search").focus()
            return false
        })

    resizeListener();
});

function recieveMessage(msg) {
    console.log(msg.data.action)
    /************ ODBIERANIE DANYCH Z IFRAME ******************/
    if(msg.data.action == "generate menu"){
        generateMenu(msg.data);
    } else if(msg.data.action == "update basket"){
        $(".tabs li.basket .qty").text("("+msg.data.qty+")");
    } else if(msg.data.action == "update userFrame"){
        var iframe = $('#basketFrame');
        iframe.attr("src", iframe.attr("src"));
    } else if(msg.data.action == "update basketFrame"){
        $(".tabs li.basket .qty").text("("+msg.data.itemsCount+")");
        var iframe = $('#basketFrame');
        iframe.attr("src", iframe.attr("src"));
    } else if(msg.data.action == "frame click"){
        $('.dropdown-button').dropdown('close');
    }
    $(".preloader").hide(0);
}
if (window.addEventListener) {
    window.addEventListener("message", recieveMessage, false);
} else {
    window.attachEvent("onmessage", recieveMessage);
}


/*************************  GENERATE MENU ***********************/
function generateMenu(data){
    var menu = data.menu;
    $(".menu ul li").remove();
    for(var a = 0; a<menu.length; a++){
        $(".menu ul").append("<li><a href='"+menu[a].url+"' target='shopFrame'>"+menu[a].label+"</a></li>");
    }
}

function resizeListener(){
    var _wh = $(window).height();
    //$(".swipes > div").css({"min-height": _wh-$('.tabs').height()})
    $(".swipes > .tabs-content, .swipes .tabs-content > div").height(_wh - $('.tabs').height() - $('.header').height());
}