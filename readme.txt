GETTING STARTED WITH GULP 
 
Step One - Install Node locally 
> http://nodejs.org 
 
Step Two - Create local "node_modules" directory and exclude it from server upload

Step Tree - Initialize npm project(root)
> npm init

Step Four  - Initialize project  and install required plugins(project directory)
> npm install --save-dev

Step Five - Ignore "node_modules" directory before commiting repository
> svn propset svn:ignore node_modules

Step Six - Run Watcher after initializing the project
> gulp watch


//////////////////////////// metody komunikacji w Json

{
  "action": "generate menu"
}

MENU
action = "generate menu

AKTUALIZACJA IKONKI KOSZYKA
action = "update basket"

AKTUALIZACJA OKNA DLA USERA
action = "update userFrame"

AKTUALIZACJA OKNA KOSZYKA
action = "update basketFrame"

Event kliku w ramce
action = "frame click"